from django.db import models

# Create your models here.


class Post(models.Model):
    title = models.CharField(max_length=100)
    create = models.DateTimeField()
    content = models.TextField()


class Comment(models.Model):
    author = models.CharField(max_length=100)
    created = models.DateTimeField()
    content = models.TextField(max_length=100)

    post = models.ForeignKey(
        "Post", related_name="comments", on_delete=models.CASCADE
    )
